# API

- host -> player stdin: commands
- player stdout -> host: events

## Conceptual

- `create/delete/suspend channel (channel props)`
- `update channel props (data)`
- `pause (channel id, pause mode) / resume (channel id)`
- `add (entry, mode) / remove (entry)`
- `save playlist / modify playlist` (modify playlist = shuffle, например)

## Host command structure

Получаем по stdin от хоста

| Field | Type | Value                  |
|-------|------|------------------------|
| op    | str  | Opcode                 |
| d     | enum | Data, opcode-dependent |

### Opcodes

| Value         | Name                  | Data | Meaning                                                                                       |
|---------------|-----------------------|------|-----------------------------------------------------------------------------------------------|
| ch_new        | Create channel        | -    | Spawn new pipe                                                                                |
| ch_del        | Delete channel        | -    | Stop playback, delete existing pipe                                                           |
| ch_upd        | Update channel props  | -    | ...                                                                                           |
| resume        | Resume playback       | -    | Resume previously paused playback                                                             |
| pause         | Pause playback        | -    | Pause playback                                                                                |
| add           | Add playlist entry    | -    | ...                                                                                           |
| del           | Remove playlist entry | -    | ...                                                                                           |
| move          | Move playlist entry   | -    | ...                                                                                           |
| state_restore | Restore playback      | -    | Restore everything related to playback on given channel (current playlist, item and position) |
| state_save    | Save playback         | -    | Save everything related to playback into player's directory                                   |
| state_reset   | Reset playback        | -    | Completely reset playback (stop and clear current play list)                                  |
| list_load     | Load playlist         | -    | Replace current playlist                                                                      |
| list_save     | Save playlist         | -    | ...                                                                                           |
| list_edit     | Modify playlist       | -    | Edit current playlist data                                                                    |

## Player event structure

Пишем в stdout хосту

| Field | Type | Value                  |
|-------|------|------------------------|
| op    | str  | Opcode                 |
| d     | enum | Data, opcode-dependent |

### Opcodes

| Value | Name            | Data | Meaning            |
|-------|-----------------|------|--------------------|
| ...   | Playback error  | -    | Any playback error |
| ...   | Playback update | -    | ...                |